import { useState, useRef, useEffect } from 'react'
import { Animated, StyleSheet, Text, View, Image, Dimensions } from 'react-native'
import bgCake from '../assets/bgicake.png'
import PrimaryButton from '../components/PrimaryButton'
import BackgroundComponent from '../components/BackgroundComponent'
import CardComponent from '../components/CardComponent';
import InfoCarouselComponent from '../components/InfoCarouselComponent';
import BirthdayInputComponent from '../components/BirthdayInputComponent';
import ArrowComponent from '../components/ArrowComponent'
import { weekdaysMin } from 'moment'

export default function HomePage({ navigation }) {
  const fadeAnimation = useRef(new Animated.Value(1)).current
  const [showInput, setShowInput ] = useState(false)
  const [date, setDate] = useState(null)
  const [buttonText, setButtonText] = useState('Get Started')
  const [isButtonEnabled, setIsButtonEnabled] = useState(true)
  const styles = useStyles()
  const onPress = () => {
    if (date)
      return navigation.navigate('Result', { date: date.toString() })

    setShowInput(true)
    setButtonText('Is Today Your Birthday?')
    setIsButtonEnabled(false)
    Animated.timing(
      fadeAnimation,
      {
        toValue: 0,
        duration: 250,
        useNativeDriver: true
      }
    ).start()
  }
  const onDateChange = (selectedDate) => {
    setDate(selectedDate)
    setIsButtonEnabled(true)
  }
  return (
      <View style={styles.container}>
        <BackgroundComponent>
            <BirthdayInputComponent show={showInput} onChange={onDateChange}/>
          <View style={styles.contentContainer}>
            <Animated.View style={{...styles.headerContainer, opacity: fadeAnimation }}>
              <CardComponent text={'Is it your\nbirthday?'} mode='header1' />
              <Animated.Image source={bgCake} style={{ ...styles.bigCake, opacity: fadeAnimation}}  pointerEvents='none' />
            </Animated.View>
            <Animated.View style={{...styles.infoContainer, opacity: fadeAnimation }}>
              <InfoCarouselComponent />
            </Animated.View>
          </View>
          <View style={styles.buttonContainer}>
            <PrimaryButton onPress={onPress} text={buttonText} isEnabled={isButtonEnabled}/>
          </View>
        </BackgroundComponent>
      </View>
    );
}

function useStyles() {
  const zIndex = 1
  const imageWidth = 400
  const imageHeight = 420
  return StyleSheet.create({
    container: {
      flex: 1,
    },
    bigCake: {
      right: -150,
      bottom: -60,
      height: imageHeight,
      width: imageWidth,
      aspectRatio: 1,
      resizeMode: 'contain',
      position: 'absolute',
    },
    contentContainer: {
      flex: 1,
      zIndex
    },
    headerContainer: {
      flex: 2, 
      justifyContent: 'flex-end', 
      paddingBottom: 11,
      paddingLeft: 25,
      paddingRight: 25,
      zIndex: 1
    },
    infoContainer: {
      flex: 2,
    },
    buttonContainer: {
      flex: 1/6,
    }
  });
}