import { StyleSheet, Text, View, ScrollView, Animated, Easing } from 'react-native';
import AnswerComponent from '../components/AnswerComponent'
import BackButtonComponent from '../components/BackButtonComponent';
import BackgroundComponent from '../components/BackgroundComponent'
import { useEffect, useState, useRef } from 'react';
import PrimaryButton from '../components/PrimaryButton';
import DealCarouselComponent from '../components/DealCarouselComponent';
import DestinationCarouselComponent from '../components/DestinationCarouselComponent';
import NotificationComponent from '../components/NotificationComponent';

export default function ResultPage({ route, navigation }) {
  const { date } = route.params
  const birthday = new Date(date)
  const fadeAnimation = useRef(new Animated.Value(0)).current
  const slideAnimation = useRef(new Animated.Value(100)).current
  const [dealHeaderText, setDealHeaderText] = useState('Check out these deals and buy yourself your own birthday present')
  const [destinationHeaderText, setDestinationHeaderText] = useState('Or treat yourself to an adventure in your local area!')
  const isToday = (day) => {
      const today = new Date()
      return day.getDate() === today.getDate() &&
      day.getMonth() === today.getMonth()
  }

  useEffect(() => {
    if (!isToday(birthday)) {
      setDealHeaderText('Treat yourself to one of our daily deals!')
      setDestinationHeaderText('Or get out and explore your local area!')
    }
    Animated.timing(
      fadeAnimation,
      {
        delay: 750,
        toValue: 1,
        duration: 500,
        useNativeDriver: true
      }
    ).start()

    Animated.timing(
      slideAnimation,
      {
        delay: 750,
        toValue: 0,
        duration: 500,
        useNativeDriver: false
      }
    ).start()
  }, []);
  const styles = useStyles()

  return (
    <BackgroundComponent>
      <ScrollView>
        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
          <BackButtonComponent />
          <NotificationComponent date={birthday}/>
        </View>
        <Animated.View style={{marginTop: slideAnimation }}>
          <AnswerComponent isTodayBirthday={isToday(birthday)}/>
          <Animated.View style={{opacity: fadeAnimation }}>
            <Text style={styles.sectionHeader}>{dealHeaderText}</Text>
            <DealCarouselComponent />
            <Text style={styles.sectionHeader}>{destinationHeaderText}</Text>
            <DestinationCarouselComponent />
            <View style={styles.buttonContainer}>
              <PrimaryButton text={'Search another date'} onPress={() => navigation.goBack()}/>
            </View>
          </Animated.View>
        </Animated.View>
      </ScrollView>
    </BackgroundComponent>
  );
}

function useStyles() {
  return StyleSheet.create({
    sectionHeader: {
      marginLeft: 25,
      marginRight: 25,
      marginTop: 25,
      marginBottom: 20,
      fontSize: 14,
      fontFamily: 'Poppins_400Regular',
    },
    buttonContainer: {
      marginTop: 30,
      marginBottom: 75
    }
  });
}