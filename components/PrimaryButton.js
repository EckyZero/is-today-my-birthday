import { StyleSheet, Text, View, TouchableOpacity, Animated, Pressable } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';

export default function PrimaryButton({ onPress, text, isEnabled = true, mode = 'primary' }) {
    const animatedButtonScale = new Animated.Value(1);
    const onPressIn = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 0.85,
            useNativeDriver: true,
        }).start();
    };
    const onPressOut = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 1,
            duration:500,
            friction:4,
            tension:50,
            useNativeDriver: true,
        }).start();
    };
    const animatedScaleStyle = {
        transform: [{scale: animatedButtonScale}]
    };
    const styles = useStyles(isEnabled, mode)
    const colors = useColors(mode)
    return (
        <Pressable style={styles.touchableArea}
            onPress={onPress}
            onPressIn={onPressIn}
            onPressOut={onPressOut}
            disabled={!isEnabled}>
            <Animated.View style={[animatedScaleStyle]}>
                <LinearGradient colors={colors}
                    style={styles.buttonLayout}
                    useAngle={true} 
                    angle={180}
                    start={{ x: 0, y: 0.4 }}
                    end={{ x: 1, y: 0.65 }} >
                        <Text style={styles.button}>{text}</Text>
                </LinearGradient>
            </Animated.View>
        </Pressable>
    )
}

function useStyles(isEnabled) {
    return StyleSheet.create({
        touchableArea: {
            textAlignVertical: 'center',
            textAlign: 'center',
            height: 54,
            marginStart: 54, 
            marginEnd: 54
        },
        button: {
            fontFamily: 'Poppins_400Regular',
            fontSize: 20,
            textAlign: 'center',
            textAlignVertical: 'center',
            color: 'white',
            paddingTop: 12
        },
        buttonLayout: {
            borderRadius: 15,
            height: 55,
            opacity: isEnabled ? 1 : 0.3
        },
    })
}

function useColors(mode) {
    if (mode === 'primary')
        return ['#6DAAEE', '#557DD9']
    else 
        return ['#D487FF', '#8330E6']
}