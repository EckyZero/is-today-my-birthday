import { useState, useEffect } from 'react'
import _ from 'lodash'
import uuid from 'react-native-uuid'
import { StyleSheet, ActivityIndicator, FlatList, Button, Linking, View } from 'react-native';
import ResultItemComponent from './ResultItemComponent';
import { syncDeals } from '../shared/sync'

export default function DealCarouselComponent() {
    const [deals, setDeals] = useState([])
    const [isError, setIsError] = useState(false)
    const styles = useStyles(isError)

    const getDeals = async () => {
        try {
            const results = await syncDeals()
            if (!results?.deals?.length) return setIsError(true)
            const items = _.sampleSize(results.deals, 10)
            setDeals(items)
        } catch (e) {
            setIsError(true)
        }
    }

    const renderDeal = ({ item, index }) => {
        return (<ResultItemComponent 
            text={item.deal.title} 
            sourceUrl={item.deal.url} 
            imageUrl={item.deal.image_url} 
            colorType={'dark'} 
            colorIndex={index}
            imageMode={'cover'}  />);
    }

    useEffect(() => {
        getDeals()
    }, []);
    return (
        <View>
            {!isError && <FlatList style={styles.container}
                directionalLockEnabled={true}
                horizontal={true}
                data={deals}
                renderItem={renderDeal}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item) => item.deal.id}
                contentInset={{ right: 40, top: 0, left: 0, bottom: 0 }}
                directionalLockEnabled={true}
                >
            </FlatList>}
            {isError && <Button
                color="#FF6B00"
                title="View Now"
                onPress={() => Linking.openURL('https://www.amazon.com/gp/goldbox')}
            />}
        </View>
    )
}

const useStyles = (isError) => {
    return StyleSheet.create({
        container: {
            paddingLeft: 15,
        },
    });
}