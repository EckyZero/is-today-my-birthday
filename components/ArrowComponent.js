import { StyleSheet, Text, View, Image } from 'react-native';
import arrow from '../assets/arrow.png'

export default function ArrowComponent({ text, direction = 'up', show = true }) {
    const styles = useStyles(direction, show)
    return (
        <View style={styles.container}>
            <Image style={styles.arrow} source={arrow} />
            <Text style={styles.text}>{text}</Text>
        </View>
    )
}

const useStyles = (direction, show) => {
    
    const position = [
        { rotate: direction === 'up' ? '0deg' : '180deg'},
        { rotateY: direction === 'up' ? '0deg' : '180deg'},
    ]
    return StyleSheet.create({
        container: {
            flexDirection: 'row',
            justifyContent: 'flex-end',
            marginRight: 20,
            opacity: show ? 1 : 0
        },
        text: {
            marginTop: 20,
            fontFamily: 'Poppins_400Regular',
            fontSize: 14,
            color: '#C18448',
        },
        arrow: {
            marginTop: 20,
            transform: position
        }
    });
}