import { memo } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { BlurView } from 'expo-blur';

function CardComponent({ text, mode, title, children }) {
    const styles = useStyles(mode);
    return (
        <View style={styles.container} >
            <BlurView intensity={10} tint={'light'}>
                {title && (<Text style={styles.title} >{title} </Text>)}
                <Text style={styles.text}>{text}</Text>
                { children }
            </BlurView>
        </View>
    )
}

function useStyles(mode) {
    const textStyle = {}
    if (mode === 'header1') {
        textStyle.fontSize = 76
        textStyle.fontFamily = 'Petemoss_400Regular'
        textStyle.textAlign = 'left'
        textStyle.paddingTop = 13
        textStyle.paddingBottom = 13
        textStyle.paddingStart = 27
        textStyle.paddingEnd = 27
    }
    else if (mode === 'header2') {
        textStyle.fontSize = 56
        textStyle.fontFamily = 'Petemoss_400Regular'
        textStyle.textAlign = 'center'
        textStyle.paddingTop = 39
        textStyle.paddingBottom = 13
        textStyle.paddingStart = 31
        textStyle.paddingEnd = 31
    }
    else {
        textStyle.fontSize = 16,
        textStyle.fontFamily = 'Poppins_400Regular'
        textStyle.textAlign = 'center'
        textStyle.padding = 27
    }

    
    return StyleSheet.create({
        container: {
            borderRadius: 28,
            backgroundColor: 'rgba(255, 45, 247, 0.056)',
            flexShrink: 1,
            overflow: 'hidden'
        },
        text: {
            textAlignVertical: 'center',
            color: '#000000',
            paddingLeft: 27,
            paddingRight: 27,
            paddingBottom: 25,
            paddingTop: 13,
            flexWrap: 'wrap',
            ...textStyle
        },
        title: {
            marginLeft: 25,
            marginTop: 25,
            fontSize: 16,
            fontFamily: 'Poppins_600SemiBold'
        }
    });
}

export default memo(CardComponent)