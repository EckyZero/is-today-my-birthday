import { StyleSheet, View, ImageBackground} from 'react-native';
import bg from '../assets/bg.png'

export default function BackgroundComponent({ children }) {
    return (
        <View style={styles.container}>
            <ImageBackground source={bg} imageStyle={{resizeMode: 'cover'}} style={styles.image}>
            {children}
            </ImageBackground>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        zIndex: 0
    },
    image: {
        flex: 1,
        justifyContent: "center",
        zIndex: 0
    }
});