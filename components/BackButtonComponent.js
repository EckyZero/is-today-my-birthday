import { StyleSheet, Text, View, Image, Animated, Pressable } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useNavigation } from '@react-navigation/native';
import backButton from '../assets/eva_arrow-back-outline.png'

export default function BackButtonComponent() {
    const navigation = useNavigation()
    const animatedButtonScale = new Animated.Value(1);
    const animatedScaleStyle = {
        transform: [{scale: animatedButtonScale}]
    };
    const styles = useStyles()
    const onPress = () => {
        navigation.goBack()
    }
    const onPressIn = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 0.85,
            useNativeDriver: true,
        }).start();
    };
    const onPressOut = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 1,
            duration:500,
            friction:4,
            tension:50,
            useNativeDriver: true,
        }).start();
    };
    return (
        <Pressable style={styles.touchableArea}
            onPress={onPress}
            onPressIn={onPressIn}
            onPressOut={onPressOut}>
            <Animated.View style={[animatedScaleStyle]}>
            <LinearGradient colors={['#6DAAEE', '#557DD9']}
                    style={styles.buttonLayout}
                    useAngle={true} 
                    angle={180}
                    start={{ x: 0, y: 0.4 }}
                    end={{ x: 1, y: 0.65 }} >
                <View style={styles.container}>
                    <Image source={backButton}/>
                </View>
                </LinearGradient>
            </Animated.View>
        </Pressable>
    )
}

function useStyles() {
    const height = 45
    const width = 45
    return StyleSheet.create({
        touchableArea: {
            marginLeft: 26,
            marginTop: 55,
            height,
            width,
            opacity: 1
        },
        buttonLayout: {
            borderRadius: 15,
            height,
            width
        },
        container: {
            alignContent: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            height,
            width
        }
    });
}