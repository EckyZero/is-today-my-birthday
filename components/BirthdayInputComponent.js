import moment from 'moment'
import { useState, useEffect, useCallback, useRef } from 'react'
import { Animated, Easing, StyleSheet, Text, View, Image, Dimensions, Pressable } from 'react-native';
import { Appearance } from 'react-native-appearance'
import CardComponent from './CardComponent';
import questionmark from '../assets/questionmark.png'
import ArrowComponent from './ArrowComponent';
import DatePicker from 'react-native-modal-datetime-picker'

export default function BirthdayInputComponent({ show, onChange }) {
    const screenHeight = Dimensions.get('window').height
    const defaultDate = moment().subtract(5, 'years').toDate()
    const [date, setDate] = useState(defaultDate)
    const [showPicker, setShowPicker] = useState(false);
    const [showArrow, setShowArrow] = useState(true)
    const [buttonText, setButtonText] = useState('mm   /   dd   /   yyyy')
    const showAnimation = useRef(new Animated.Value(-screenHeight)).current;
    const isDarkModeEnabled = colorScheme === 'dark'
    const styles = useStyles(isDarkModeEnabled)
    const colorScheme = Appearance.getColorScheme()
    const onPress = (event) => {
        setShowPicker(true)
    }
    const onConfirm = (selectedDate) => {
        setShowArrow(false)
        setShowPicker(false);    
        setDate(selectedDate);
        setButtonText(`${addLeadingZero(selectedDate.getMonth() + 1)}   /   ${addLeadingZero(selectedDate.getDate())}   /   ${selectedDate.getFullYear()}`)
        onChange(selectedDate)
    };
    const onCancel = () => {
        setShowPicker(false)
    };
    const addLeadingZero = (number) => {
        return number < 10 ? `0${number}` :`${number}`
    }
    useEffect(() => {
        Animated.spring(showAnimation, {
                toValue: show ? -100 : -screenHeight,
                duration:show ? 1250 : 0,
                friction:5,
                tension:20,
                useNativeDriver: true,
                isDarkModeEnabled:{isDarkModeEnabled}
            }).start();
    },[show])
    return (
        <Animated.View style={{ ...styles.container, transform: [{ translateY: showAnimation }] }}>
            <View style={styles.image}>
                <Image source={questionmark} />
            </View>
            <CardComponent mode='header2' text='Enter your Birthday'>
                <Pressable style={styles.touchableArea} onPress={onPress} accessible={true}>
                    <Text style={styles.text}>{buttonText}</Text>
                </Pressable>
            </CardComponent>
                <ArrowComponent text='Enter your Birthday first' show={showArrow}/>
            <DatePicker
                isVisible={showPicker}
                mode="date"
                date={date}
                onConfirm={onConfirm}
                onCancel={onCancel}
                minimumDate={moment().subtract(100, 'years').toDate()}
                disabled={false}
                maximumDate={new Date()}
            />
        </Animated.View>
    )
}
function useStyles(isDarkModeEnabled) {
    return StyleSheet.create({
        container: {
            position: 'absolute',
            left: 25,
            right: 25,
            alignItems: 'center',
            justifyContent: 'center',
            zIndex: 2
        },
        touchableArea: {
            textAlignVertical: 'center',
            textAlign: 'center',
            height: 54,
            marginStart: 23, 
            marginEnd: 23,
            backgroundColor: '#FEC792',
            borderRadius: 15,
            height: 55,
            marginTop: 10,
            marginBottom: 29,
            justifyContent: 'center',
            borderRadius: 55/2,
            zIndex: 99999, 
            elevation: 99999,
        },
        image: {
            marginBottom: -35,
            zIndex: 1,
            alignItems: 'center',
        },
        arrow: {
            marginTop: 20,
            marginLeft: -100,
        },
        text: {
            fontFamily: 'Poppins_400Regular',
            fontSize: 20,
            textAlign: 'center',
        }
    });
}