import { useEffect } from 'react'
import { StyleSheet, Text, View, Image, Animated } from 'react-native';
import { BlurView } from 'expo-blur';
import cake1 from '../assets/Frame.png'

export default function AnswerComponent({ isTodayBirthday }) {
    const startValue = new Animated.Value(0.0);
    const endValue = 1.0
    const styles = useStyles(isTodayBirthday);
    const title = isTodayBirthday ? 'Yes, it is!' : 'Oh no! It\'s not!'
    const subtitle = isTodayBirthday ? 'So here are some fun ideas to celebrate!' : `But here are some fun ideas \nto kill some time while you wait!`
    const animatedScaleStyle = {
        transform: [{scale: startValue}]
    };
    useEffect(() => {
        Animated.spring(startValue, {
            toValue: endValue,
            friction: 5,
            useNativeDriver: true,
        }).start();
      }, [startValue, endValue]);
    return (
        <Animated.View 
            style={[styles.container, animatedScaleStyle]} 
            pointerEvents='none'>
            <View style={styles.imageContainer}>
                {isTodayBirthday && (<Image style={styles.image}source={cake1} />)}
            </View>
            <View style={styles.blurContainer}>    
                <BlurView style={styles.textContainer} intensity={10} tint={'light'}>
                    <Text style={styles.title}>{title}</Text>
                    <Text style={styles.subtitle}>{subtitle}</Text>
                </BlurView>
            </View>
        </Animated.View>
    )
}

function useStyles(isTodayBirthday) {
    return StyleSheet.create({
        container: {
            marginTop: isTodayBirthday ? -65 : 18,
            marginEnd: 25,
            marginLeft: 25,
        },
        imageContainer: {
            alignItems: 'center',
            zIndex: 1
        },
        blurContainer: {
            borderRadius: 28,
            overflow: 'hidden'
        },
        textContainer: {
            backgroundColor: 'rgba(255, 45, 247, 0.056)',
            flexShrink: 1,
            alignItems: 'center',
            zIndex: -1
        },
        image: {
            position: 'relative',
            bottom: -40
        },
        title: {
            textAlignVertical: 'center',
            color: '#000000',
            flexWrap: 'wrap',
            fontSize: 72,
            fontFamily: 'Petemoss_400Regular',
            textAlign: 'left',
            paddingTop: isTodayBirthday ? 40 : 25,
            paddingStart: 19,
            paddingEnd: 19,
            paddingBottom: 12
        },
        subtitle: {
            fontFamily: 'Poppins_400Regular',
            fontSize:14,
            paddingBottom: 32,
            textAlign: 'center'
        },
    });
}