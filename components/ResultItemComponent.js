import { StyleSheet, Text, View, Linking, Image, Pressable } from 'react-native';
import { LinearGradient } from 'expo-linear-gradient';
import { useState } from 'react';
import placeholderImage from '../assets/image-s.png'

const gradients = {
    dark: [
        ['#FF7B89', '#8A5082'],
        ['#8A5082', '#6F5F90'],
        ['#6F5F90', '#758EB7'],
        ['#758EB7', '#A5CAD2'],
        ['#A5CAD2', '#FF7B89'],
        ['#FF7B89', '#8A5082'],
        ['#8A5082', '#6F5F90'],
        ['#6F5F90', '#758EB7'],
        ['#758EB7', '#A5CAD2'],
        ['#A5CAD2', '#FF7B89']
    ],
    light: [
        ['#D7EDFA', '#F3F3FC'],
        ['#F3F3FC', '#E9E1F9'],
        ['#E9E1F9', '#FAF4ED'],
        ['#FAF4ED', '#F6DFDA'],
        ['#F6DFDA', '#D7EDFA'],
        ['#D7EDFA', '#F3F3FC'],
        ['#F3F3FC', '#E9E1F9'],
        ['#E9E1F9', '#FAF4ED'],
        ['#FAF4ED', '#F6DFDA'],
        ['#F6DFDA', '#D7EDFA']
    ]
}

export default function ResultItemComponent({ text, sourceUrl, imageUrl, imageMode = 'center', colorType = 'dark', colorIndex = 1}) {
    const styles = useStyles(colorType, imageMode)
    const colors = gradients[colorType][colorIndex]
    const [imageError, setImageError] = useState(false)
    const onImageError = (event) => {
        setImageError(true)
    }
    return (
        <View style={styles.container}>
            <View style={styles.imageContainer}>
                <Image style={styles.image} source={(imageError | !imageUrl) ? placeholderImage : {uri: imageUrl}} onError={onImageError}/>
            </View>
            <LinearGradient colors={colors}
                style={styles.contentContainer}
                useAngle={true} 
                angle={180}
                start={{ x: 0, y: 0.4 }}
                end={{ x: 1, y: 0.65 }} >
                    <Text style={styles.text}>{text}</Text>
                    <Pressable onPress={() => Linking.openURL(sourceUrl)}>
                        <Text style={styles.button}>See More</Text>
                    </Pressable>
            </LinearGradient>
        </View>
    )
}

const useStyles = (colorType, imageMode) => {
    const textColor = colorType === 'dark' ? 'white' : 'black'
    const buttonColor = colorType === 'dark' ? 'white' : '#FF6B00'
    return StyleSheet.create({
        container: {
            alignItems: 'center',
            marginLeft: 10
        },
        contentContainer: {
            height: 150,
            width: 176,
            borderRadius: 28,
            borderWidth: 2.5,
            borderColor: 'white',
            flex: 1,
            paddingBottom: 15,
            paddingLeft: 10,
            paddingRight: 10,
            paddingTop: 50,
        },
        text: {
            textAlign: 'center',
            fontSize: 10,
            fontFamily: 'Poppins_300Light',
            flex: 1,
            color: textColor
        },
        touchableArea: {
            flex: 1
        },
        button: {
            textAlign: 'center',
            fontSize: 14,
            fontFamily: 'Poppins_600SemiBold',
            color: buttonColor
        },
        image: {
            backgroundColor: 'white',
            resizeMode: imageMode,
            flex: 1
        },
        imageContainer: {
            zIndex: 1,
            marginBottom: -40,
            height: 109,
            width: 126,
            borderRadius: 15,
            overflow: 'hidden'
        }
    });
}