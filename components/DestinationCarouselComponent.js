import _ from 'lodash'
import { useState, useEffect } from 'react'
import { StyleSheet, FlatList, View, Button, Linking } from 'react-native';
import ResultItemComponent from './ResultItemComponent';
import { syncDestinations } from '../shared/sync'

export default function DestinationCarouselComponent() {
    const [destinations, setDestinations] = useState([])
    const [isError, setIsError] = useState(false)
    const styles = useStyles()

    const getDestinations = async () => {
        try {
            const events = await syncDestinations()
            if (!events?.businesses?.length) return setIsError(true)
            const items = _.sampleSize(events.businesses, 10)
            console.log({ items })
            setDestinations(items)
        } catch (e) {
            console.log({ e })
            setIsError(true)
        }
    }

    const renderDestination = ({ item, index }) => {
        return (<ResultItemComponent 
            text={item.name} 
            sourceUrl={item.url} 
            imageUrl={item.image_url} 
            colorType={'light'} 
            colorIndex={index}
            imageMode={'cover'} />);
    }

    useEffect(() => {
        getDestinations()
    }, []);
    return (
        <View>
            {!isError && <FlatList style={styles.container}
                directionalLockEnabled={true}
                horizontal={true}
                data={destinations}
                renderItem={renderDestination}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={(item) => item.id}
                contentInset={{ right: 40, top: 0, left: 0, bottom: 0 }}
                directionalLockEnabled={true}
                >
            </FlatList>}
            {isError && <Button
                color="#FF6B00"
                title="View Now"
                onPress={() => Linking.openURL('https://www.yelp.com/nearme/local-flavor')}
            />}
        </View>
    )
}

const useStyles = () => {
    return StyleSheet.create({
        container: {
            paddingLeft: 15,
            paddingRight: 15
        }
    });
}