import React, { useState, useCallback, useRef } from 'react';
import { Dimensions, View, StyleSheet, Image } from 'react-native';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import CardComponent from './CardComponent';
import circle1 from '../assets/circle1.png'

const carouselItems = [
  {
    text: 'In the time it takes you to read this, another five babies will have been born',
  },
  {
    text: 'More people celebrate their birthdays in August than in any other month',
  },
  {
    text: 'The most common birth date is October 5th, which falls 9 months after New Year\'s Eve'
  },
  {
    text: 'The least common birth date in the U.S. is May 22nd (not counting leap years)'
  },
  {
    text: 'Cards that play a song use more computing power than was used to send man to the moon'
  },
];

export default function InfoCarouselComponent () {
  const [activeIndex, setActiveIndex] = useState(0);
  const ref = useRef(null);
  const width = Dimensions.get('window').width

  const renderItem = useCallback(({ item, index }) => (
    <View style={styles.container}>
      <CardComponent text={item.text} title={'Did you know?'}></CardComponent>
    </View>
  ), []);

  return (
      <View >
        <Carousel
          layout="default"
          ref={ref}
          data={carouselItems}
          sliderWidth={width}
          itemWidth={width}
          renderItem={renderItem}
          onSnapToItem={(index) => setActiveIndex(index)}
          loop={true}
          inactiveSlideOpacity={1}
          inactiveSlideScale={1}
          enableMomentum={true}
          decelerationRate={0.9}
          enableSnap={true}
        />
        <View style={{ marginTop: 0}}>
          <Pagination
            dotsLength={carouselItems.length}
            activeDotIndex={activeIndex}
            inactiveDotElement={<View style={{
              backgroundColor: "#E9E9E9",
              height: 13,
              width: 13,
              borderRadius: 13/2,
              borderWidth: 0,
              marginRight: 7,
              marginLeft: 7
            }}></View>}
            dotElement={ActiveDot()}
            inactiveDotOpacity={1}
            inactiveDotScale={1}
          />
        </View>
      </View>
  );
};

const styles = StyleSheet.create({
  container: {
    marginLeft: 25,
    marginRight: 25
  },
});



function ActiveDot () {
  return (
    <Image source={circle1} style={{ width: 23, height: 23 }} />
  )
}