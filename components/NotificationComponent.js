import { StyleSheet, View, Alert, Animated, Pressable } from 'react-native';
import { useState } from 'react';
import { LinearGradient } from 'expo-linear-gradient';
import { Ionicons } from '@expo/vector-icons';
import { PermissionStatus } from 'expo-modules-core';
import * as Notifications from 'expo-notifications';
import moment from 'moment'

export default function NotificationComponent({ date }) {
    const [isScheduled, setIsScheduled] = useState(false)
    const styles = useStyles()
    const animatedButtonScale = new Animated.Value(1);
    const animatedScaleStyle = {
        transform: [{scale: animatedButtonScale}]
    };
    const schedule = async () => {
        const { status } = await Notifications.requestPermissionsAsync();

        if (status !== PermissionStatus.GRANTED) {
            Alert.alert('Need Notification Permissions', 'Go to your device settings and grant us permission to send you notifications', [
                { text: 'OK' },
            ]);
            return
        };
        const reminder = moment(date)
        const schedulingOptions = {
            content: {
                title: 'Today is your birthday!',
                body: 'Check out the special deals we have planned for you!',
                sound: true,
                priority: Notifications.AndroidNotificationPriority.HIGH,
            },
            trigger: {
                day: reminder.date(),
                month: reminder.month() + 1,
                hour: 8,
                minute: 30
            },
        };
        Notifications.scheduleNotificationAsync(schedulingOptions);

        Alert.alert('Alright!', 'We\'ll remind you when it\'s your birthday', [
            { text: 'OK' },
        ]);

        setIsScheduled(true)
    }

    const onPress = async () => {
        if (isScheduled) {
            Alert.alert('You already asked us to remind you!', 'Don`t worry, we won\'t forget :)', [
                { text: 'OK' },
            ]);
            return
        }
        Alert.alert('Would you like us to remind you when it\'s your birthday?', 'You\'ll need to give us permission to do so', [
            { text: 'Cancel', style: 'cancel' },
            { text: 'OK', onPress: schedule },
        ]);
    }
    const onPressIn = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 0.85,
            useNativeDriver: true,
        }).start();
    };
    const onPressOut = () => {
        Animated.spring(animatedButtonScale, {
            toValue: 1,
            duration:500,
            friction:4,
            tension:50,
            useNativeDriver: true,
        }).start();
    };
    return (
        <Pressable style={styles.touchableArea}
            onPress={onPress}
            onPressIn={onPressIn}
            onPressOut={onPressOut}>
            <Animated.View style={[animatedScaleStyle]}>
            <LinearGradient colors={['#FEC792', '#FF6B00']}
                    style={styles.buttonLayout}
                    useAngle={true} 
                    angle={180}
                    start={{ x: 0, y: 0.4 }}
                    end={{ x: 1, y: 0.65 }} >
                <View style={styles.container}>
                    <Ionicons name="notifications" size={24} color="white" />
                </View>
                </LinearGradient>
            </Animated.View>
        </Pressable>
    )
}

function useStyles() {
    const height = 45
    const width = 45
    return StyleSheet.create({
        touchableArea: {
            marginRight: 26,
            marginTop: 55,
            height,
            width,
            opacity: 1
        },
        buttonLayout: {
            borderRadius: 15,
            height,
            width
        },
        container: {
            alignContent: 'center',
            justifyContent: 'center',
            alignItems: 'center',
            height,
            width
        }
    });
}