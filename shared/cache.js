import AsyncStorage from '@react-native-async-storage/async-storage';

export async function set(key, value) {
    return AsyncStorage.setItem(key, JSON.stringify(value))
}

export async function get(key) {
    const value = await AsyncStorage.getItem(key)
    if (!value) return
    else return JSON.parse(value)
}