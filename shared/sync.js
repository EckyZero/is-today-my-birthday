
import { get, set } from './cache'
import moment from 'moment'
import * as rssParser from 'react-native-rss-parser';
import Constants from 'expo-constants';

export async function fetchThroughCache(url, responseType = 'json', options) {
    const now = moment().format('l')
    const cacheKey = `${now}:${url}`
    const result = await get(cacheKey)

    if(result) return result

    let results
    try {
        const response = await fetch(url, options)
        if (responseType === 'xml') {
            const data = await response.text()
            results = await rssParser.parse(data)
        }
        else {
            results = await response.json()
        }
    } catch (e) {
        console.log(e)
    }
    await set(cacheKey, results)
    return results
}

export async function syncDeals() {
    return fetchThroughCache(`https://api.discountapi.com/v2/deals?api_key=${Constants.manifest.extra.DEALS_API_KEY}&online=true&order=updated_at_desc`)
}

export async function syncDestinations() {
    const location = await fetchThroughCache('https://ipapi.co/json/')
    if (!location) return
    return fetchThroughCache(`https://api.yelp.com/v3/businesses/search?latitude=${location.latitude}&longitude=${location.longitude}&categories=localflavor&limit=10`, 'json', {
        method: 'GET',
        headers: {
            'Authorization': `Bearer ${Constants.manifest.extra.YELP_API_KEY}`
        }
    })
}

export async function syncAll() {
    await syncDeals()
    await syncDestinations()
}