export default {
    expo: {
        name: "My Birthday",
        slug: "istodaymybirthday",
        version: "1.0.9",
        orientation: "portrait",
        icon: "./assets/icon.png",
        userInterfaceStyle: "automatic",
        extra: {
            YELP_API_KEY: process.env.YELP_API_KEY,
            DEALS_API_KEY: process.env.DEALS_API_KEY
        },
        splash: {
            image: "./assets/splash.png",
            resizeMode: "contain",
            backgroundColor: "#ffffff"
        },
        updates: {
            "fallbackToCacheTimeout": 0
        },
        assetBundlePatterns: [
            "**/*"
        ],
        ios: {
            supportsTablet: false,
            userInterfaceStyle: "light",
            bundleIdentifier: "com.erikandersen.istodaymybirthday",
            buildNumber: "1.0.9"
        },
        android: {
            adaptiveIcon: {
            foregroundImage: "./assets/icon.png",
            backgroundColor: "#FFFFFF"
            },
            package: "com.erikandersen.istodaymybirthday",
            userInterfaceStyle: "light",
            permissions: ['READ_EXTERNAL_STORAGE', 'WRITE_EXTERNAL_STORAGE', 'INTERNET'],
            versionCode: 4
        },
        web: {
            favicon: "./assets/favicon.png"
        },
        sdkVersion: "44.0.0"
    }
}