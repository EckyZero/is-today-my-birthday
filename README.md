# Is Today My Birthday?

## Now Available for [iOS](https://apps.apple.com/az/app/is-today-my-birthday/id1609714165) and [Android](https://play.google.com/store/apps/details?id=com.erikandersen.istodaymybirthday&hl=en_US&gl=US)

[![](./docs/hero.jpg)](https://youtu.be/t4D25afTklM "Is Today My Birthday Demo")

**Click [here](https://youtu.be/t4D25afTklM) to see a demo**

Finally! The answer to one of life's deepest questions...

The kind that keeps you awake at night... The kind that causes you to break out in a cold sweat whenever you remember that your special time of year draws near

Rest assured, those sleepless nights are now over!

Download and find out - Is Today Your Birthday?!

### Description

**Is Today my Birthday** is a cross-platform mobile app written in JavaScript and TypeScript and uses React Native and [Expo](https://expo.dev/). It uses Yelp and Groupon REST APIs to source activities and deals for a selected date

It uses a custom CI/CD pipeline hosted in [Bitrise](https://www.bitrise.io/) that generates App Store ready builds on every commit to main

### How to Run

#### Pre-requisites

- For iOS - install Xcode, Xcode command line tools, and at least 1 simulator configured
- For Android - install Android Studio and have at least 1 emulator configured

#### Local Development

1. Get an API Key for Groupon - place in the `DEALS_API_KEY` field in `sync.js`
2. Get an API Key for YELP - place in the `YELP_API_KEY` field in `sync.js`
3. `npm install`
4. `npm run start`
5. When prompted, `i` for `ios` or `a` for android
6. Done!
