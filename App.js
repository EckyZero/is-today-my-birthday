import { useEffect } from 'react'
import { NavigationContainer } from '@react-navigation/native';
import HomePage from './pages/HomePage'
import ResultPage from './pages/ResultPage'
import AppLoading from 'expo-app-loading';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { useFonts, Poppins_400Regular, Poppins_600SemiBold, Poppins_300Light } from '@expo-google-fonts/poppins';
import { Petemoss_400Regular } from '@expo-google-fonts/petemoss';
import { StatusBar } from 'expo-status-bar';
import { syncAll } from './shared/sync'

const Stack = createNativeStackNavigator();

export default function App() {
  let [fontsLoaded] = useFonts({
    Poppins_400Regular,
    Poppins_600SemiBold,
    Petemoss_400Regular,
    Poppins_300Light
  });
  useEffect(() => {
    syncAll()
  }, []);
  if (!fontsLoaded) {
    return <AppLoading />;
  }
  return (
    <NavigationContainer>
      <StatusBar style="dark" />
      <Stack.Navigator>
        <Stack.Screen name="Home" component={HomePage} options={{ headerShown: false }} />
        <Stack.Screen name="Result" component={ResultPage}  options={{ headerShown: false }}/>
      </Stack.Navigator>
    </NavigationContainer>
  );
}